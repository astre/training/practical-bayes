# Régression Bayésienne en pratique <img src="img/logo-bayes.png" align="right" width="200px"/>

Formation de 4 jours, d'introduction à la modélisation Bayésienne, avec une approche opérationnelle.


## Planning

![](img/plan.png)



## Workflow

Ce dépôt contient le code pour le site web de la formation, ainsi que pour les matériaux à diffuser (diapositives, exercices, activités, consignes, programme, etc...).

Les documents administratifs et de travail se trouvent dans l'espace SharePoint [Practical Bayes dev](https://ciradfr.sharepoint.com/sites/PracticalBayesdev160/Documents%20partages/Forms/AllItems.aspx?id=%2Fsites%2FPracticalBayesdev160%2FDocuments%20partages%2FGeneral&viewid=b811290e%2D2558%2D4068%2D975a%2Da99e2c97ffb1), avec un accès restraint aux membres du groupe.

Nous allons nous servir de cette plateforme pour la planification et la gestion du projet de développement. Notamment en définissant des [milestones](https://gitlab.cirad.fr/astre/training/practical-bayes/-/milestones) et des [tâches](https://gitlab.cirad.fr/astre/training/practical-bayes/-/issues).


---
![](img/banner-logos.png)
Avec le support de [![Agreenium](img/agreenium.png)](https://www.agreenium.fr/)